import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { LoginService } from '/Users/lokesh/personal/ionic/demoapp/src/pages/services/LoginService';


@Component({
  selector: 'page-managerlogin',
  templateUrl: 'managerlogin.html'
})
export class ManagerLoginPage {

	username:string;
	password:string;

  constructor(public navCtrl: NavController,public loginService: LoginService) {

  }

  login(){
  	console.log('Username is '+this.username);
  	console.log('Password is '+this.password);


    this.loginService.login(this.username,this.password,"admin").subscribe(
                data => {
                    console.log(data);
                    this.navCtrl.push(DashboardPage,{
                      usertype:'admin',
                      username: this.username
                    });
                },
                err => {
                    console.log(err);
                }
            );

    }


  }
