import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AgentDashboardPage } from '../agentdashboard/agentdashboard';
import { LoginService } from '/Users/lokesh/personal/ionic/demoapp/src/pages/services/LoginService';

@Component({
  selector: 'page-agentlogin',
  templateUrl: 'agentlogin.html'
})
export class AgentLoginPage {

	username:string;
	password:string;

  constructor(public navCtrl: NavController,public loginService: LoginService) {

  }

  login(){
  	console.log('Username is '+this.username);
  	console.log('Password is '+this.password);

    this.loginService.login(this.username,this.password,"agent").subscribe(
                data => {
                    console.log(data);
                    this.navCtrl.push(AgentDashboardPage,{
                      usertype:'agent',
                      username: this.username
                    });
                },
                err => {
                    console.log(err);
                }
            );

    }

}
