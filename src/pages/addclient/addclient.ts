import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { NavController,NavParams } from 'ionic-angular';
import { LoginService } from '/Users/lokesh/personal/ionic/demoapp/src/pages/services/LoginService';


@Component({
  selector: 'page-addclient',
  templateUrl: 'addclient.html'
})
export class AddClientPage {

  name:string;
  email:string;
  location:string;
  duration:string;
  status:string;  
  usertype:string;
  username:string;


  constructor(public navCtrl: NavController,private alertCtrl: AlertController,public loginService: LoginService,public navParams: NavParams) {

      this.usertype = navParams.get('usertype');
      this.username = navParams.get('username');

  }

  ionViewWillEnter(){
      this.status = "started";
  }

  addClient(){

    console.log('here'+this.usertype);
    
    this.loginService.createClient(this.name,this.email,this.location,this.duration,this.status,this.usertype,this.username).subscribe(
                data => {
                    this.presentAlert();
                },
                err => {
                    console.log();
                }
            );

    }

    presentAlert() {
      let alert = this.alertCtrl.create({
       title: 'Success',
        subTitle: 'Client Created',
      buttons: [
      {
        text: 'Ok',
        role: 'Ok',
        handler: () => {
          this.navCtrl.pop();
          console.log('Ok clicked');
        }
      }
      ]
      });
      alert.present();
}





  }






