import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { AddClientPage } from '../addclient/addclient';
import { LoginService } from '/Users/lokesh/personal/ionic/demoapp/src/pages/services/LoginService';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

	clients: Array<any>;
	usertype: String;
	username: String;

  constructor(public navCtrl: NavController,public navParams: NavParams,public loginService: LoginService) {

  		this.usertype = navParams.get('usertype');
  		this.username = navParams.get('username');
  		var queryParam = "agent=agent1";

  		this.loginService.clients(queryParam).subscribe(
                data => {
                console.log(data);
                this.clients = data;
                },
                err => {
                    console.log(err);
                }
            );
  }


  openAddClient(){
 		this.navCtrl.push(AddClientPage,{
 			usertype:'admin',
            username: this.username
 		});
 	}

 

}
