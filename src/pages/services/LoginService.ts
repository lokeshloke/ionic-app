import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Http, Headers, RequestOptions} from '@angular/http';
 
@Injectable()
export class LoginService {
 
    constructor(private http:Http) {
 
    }

    getHeaders(){
    	const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});
        return options;
    }
 
    login(uname,ps,atype) {
    	var parameter = JSON.stringify({userType:atype, username:uname, password:ps});
        var url = 'http://192.168.1.100:3000/auth';
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});
        var response = this.http.post(url,parameter,options);
        return response;
    }   

    clients(queryParams){
    	var url = 'http://192.168.1.100:3000/getClients?'+queryParams;
    	var response = this.http.get(url,this.getHeaders()).map(res=>res.json());
    	return response;
    }

    createClient(cname,cemail,clocation,cduration,cstatus,usertype,uname){
        var postParams = JSON.stringify({name:cname, email:cemail,  
                                            location:clocation,duration:cduration,status:cstatus,'agent':uname,userType:'client'});
    	var url = 'http://192.168.1.100:3000/createOrUpdateClient';
    	var response = this.http.post(url,postParams,this.getHeaders());
    	return response;
    }

}