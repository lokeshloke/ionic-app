import { Component } from '@angular/core';

import { AgentLoginPage } from '../agentlogin/agentlogin';
import { ManagerLoginPage } from '../managerlogin/managerlogin';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  agentRoot = AgentLoginPage;
  managerRoot = ManagerLoginPage;
  constructor() {

  }
}
