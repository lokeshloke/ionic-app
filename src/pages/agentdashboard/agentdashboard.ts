import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { AddClientPage } from '../addclient/addclient';
import { LoginService } from '/Users/lokesh/personal/ionic/demoapp/src/pages/services/LoginService';
import { Subscription } from 'rxjs';




@Component({
  selector: 'page-dashboard',
  templateUrl: 'agentdashboard.html'
})
export class AgentDashboardPage {

	clients: Array<any>;
	usertype: String;
	username: String;


  constructor(public navCtrl: NavController,public navParams: NavParams,public loginService: LoginService) {
  		
  		this.usertype = navParams.get('usertype');
  		this.username = navParams.get('username');
  }

  ionViewWillEnter(){
 	 this.fetchClients();
  }

    fetchClients(){
    	console.log('Fetching CLients');
  		var queryParam = this.usertype+"="+this.username;
    	this.loginService.clients(queryParam).subscribe(
                data => {
                console.log(data);
                this.clients = data;
                },
                err => {
                    console.log(err);
                }
            );
    }
 
 	openAddClient(){
 		this.navCtrl.push(AddClientPage,{
 			usertype:'agent',
            username: this.username
 		});
 	}

}
