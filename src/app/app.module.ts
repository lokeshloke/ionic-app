import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { AgentLoginPage } from '../pages/agentlogin/agentlogin';
import { ManagerLoginPage } from '../pages/managerlogin/managerlogin';
import {AddClientPage} from '../pages/addclient/addclient';
import {DashboardPage} from '../pages/dashboard/dashboard';
import {AgentDashboardPage} from '../pages/agentdashboard/agentdashboard';
import { HttpModule } from '@angular/http';
import { LoginService } from '/Users/lokesh/personal/ionic/demoapp/src/pages/services/LoginService';

@NgModule({
  declarations: [
    MyApp,
    AgentLoginPage,
    TabsPage,
    ManagerLoginPage,
    AddClientPage,
    DashboardPage,
    AgentDashboardPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    AgentLoginPage,
    ManagerLoginPage,
    AddClientPage,
    DashboardPage,
    AgentDashboardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LoginService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
